#!/bin/bash

base_dir=`pwd`

for env in "$@"; do

	if grep TODO ${base_dir}/newrelic-alerting/onprem/service.yaml ; then
		echo "Check service data"
	else
		case $env in
			pp[123] )
				echo "Applying to $env"
				auth_string="aws-okta exec trd-dev-write --"
				;;

			pt1 )
				echo "Applying to $env"
				auth_string="aws-okta exec trd-nonprod-write --"
				;;

			prd )
				echo "Applying to $env"
				auth_string="aws-okta exec trd-prod-write --"
				;;

			* )
				echo "Please supply an OnPrem environmnent"
				auth_string="BAD"
				;;
		esac
		if [ "$auth_string" == "BAD" ]; then
			echo "Please supply a real, on prem environment"
		else
			cd ${base_dir}/newrelic-alerting/onprem/${env}
			pwd
			$auth_string terragrunt apply --auto-approve
			pwd
			cd ${base_dir}
		fi
	fi
done
